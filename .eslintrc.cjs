module.exports = {
  // 指定运行环境
  env: {
    browser: true, // 浏览器环境
    es2021: true, // ECMAScript 2021
    node: true, // Node.js 环境
    jest: true, // 支持 Jest 测试框架
  },
  // 指定解析器
  parser: '@babel/eslint-parser', // 使用 Babel 解析器来解析代码
  // 解析器选项
  parserOptions: {
    ecmaVersion: 'latest', // 支持最新的 ECMAScript 版本
    sourceType: 'module', // 使用 ES6 模块
    ecmaFeatures: {
      jsx: true, // 启用 JSX 语法支持
    },
  },
  // 继承已有的规则
  extends: [
    'eslint:recommended', // 使用 ESLint 推荐的规则
    'plugin:react/recommended', // 使用 React 推荐的规则
    'plugin:@typescript-eslint/recommended', // 使用 TypeScript 推荐的规则
    'plugin:prettier/recommended', // 使用 Prettier 推荐的规则，禁用与 Prettier 冲突的 ESLint 规则
  ],
  // 使用的插件
  plugins: [
    'react', // React 插件
    '@typescript-eslint', // TypeScript 插件
  ],
  // 自定义规则
  rules: {
    // ESLint 常规规则
    'no-var': 'error', // 要求使用 let 或 const 而不是 var
    'no-multiple-empty-lines': ['warn', { max: 1 }], // 不允许多个空行
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off', // 禁用 console (生产环境)
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off', // 禁用 debugger (生产环境)
    'no-unexpected-multiline': 'error', // 禁止空余的多行
    'no-useless-escape': 'off', // 禁止不必要的转义字符

    // TypeScript 规则
    '@typescript-eslint/no-unused-vars': 'error', // 禁止定义未使用的变量
    '@typescript-eslint/prefer-ts-expect-error': 'error', // 推荐使用 @ts-expect-error 而非 @ts-ignore
    '@typescript-eslint/no-explicit-any': 'off', // 允许使用 any 类型
    '@typescript-eslint/no-non-null-assertion': 'off', // 允许使用非空断言
    '@typescript-eslint/no-namespace': 'off', // 允许使用自定义 TypeScript 模块和命名空间
    '@typescript-eslint/semi': 'off', // 关闭 TypeScript 的分号规则（与 Prettier 冲突）

    // React 规则
    'react/react-in-jsx-scope': 'off', // 在 React 17+ 中不再需要显式导入 React
    'react/jsx-uses-react': 'off', // 禁用不再需要的规则（React 17+）
    'react/jsx-uses-vars': 'error', // 防止 JSX 中的变量被标记为未使用
    'react/prop-types': 'off', // 禁用 prop-types 检查（通常在 TypeScript 中不需要）
  },
  // React 版本自动检测
  settings: {
    react: {
      version: 'detect', // 自动检测 React 版本
    },
  },
};