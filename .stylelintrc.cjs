module.exports = {
    extends: [
      'stylelint-config-standard', // 配置stylelint标准规则集
      'stylelint-config-standard-scss', // 配置stylelint SCSS规则集
      'stylelint-config-recess-order', // 配置stylelint CSS属性顺序规则
      'stylelint-config-prettier', // 配置stylelint与prettier兼容
    ],
    overrides: [
      {
        files: ['**/*.(scss|css|jsx|tsx|js)'], // 适用于 SCSS、CSS、JSX、TSX 和 JS 文件
        customSyntax: 'postcss-scss', // 使用 postcss-scss 作为语法解析器
      },
    ],
    ignoreFiles: [
      '**/*.json',
      '**/*.md',
      '**/*.yaml',
    ],
    rules: {
      'value-keyword-case': null, // 关闭大小写检查，避免在样式中使用自定义变量时出错
      'no-descending-specificity': null, // 允许具有较高优先级的选择器后面出现较低优先级的选择器
      'function-url-quotes': 'always', // 要求 URL 使用引号
      'no-empty-source': null, // 允许空的样式文件
      'selector-class-pattern': null, // 关闭对选择器类名格式的强制要求
      'property-no-unknown': null, // 允许未知的属性（避免自定义属性出错）
      'block-opening-brace-space-before': 'always', // 大括号前必须有空格
      'value-no-vendor-prefix': null, // 允许属性值使用浏览器前缀
      'property-no-vendor-prefix': null, // 允许属性使用浏览器前缀
      'selector-pseudo-class-no-unknown': [
        true,
        {
          ignorePseudoClasses: ['global', 'v-deep'], // 忽略一些常见的伪类
        },
      ],
    },
  };